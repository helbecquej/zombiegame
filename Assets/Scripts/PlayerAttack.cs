﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField]
    KeyCode _keyToAttack = KeyCode.Mouse0;
    [SerializeField]
    Transform _sword;
    [SerializeField]
    float _speed = 3f;

    private Vector3 targetAngle = new Vector3(150f, 0f, 0f);

    private Vector3 currentAngle;

    private Coroutine _attackCoroutine;

    public void Start()
    {
        currentAngle = new Vector3(130f, 0f, 0f);
    }

    void Update()
    {
        if (Input.GetKeyDown(_keyToAttack))
        {
            if (_attackCoroutine != null)
                StopCoroutine(_attackCoroutine);
            currentAngle = new Vector3(130f, 0f, 0f);
            _attackCoroutine = StartCoroutine(AttackCoroutine());
        }
    }

    private void OnDestroy()
    {
        StopCoroutine(_attackCoroutine);
    }

    IEnumerator AttackCoroutine()
    {
        while (Mathf.Abs(currentAngle.x - 150f) > 0.1f)
        {
            currentAngle = new Vector3(Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime * _speed), 0f, 0f);

            _sword.localEulerAngles = currentAngle;
            yield return null;
        }
        targetAngle.x = 130f;
        while (true)
        {
            currentAngle = new Vector3(Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime * _speed), 0f, 0f);

            _sword.localEulerAngles = currentAngle;
            yield return null;
        }
    }
}
