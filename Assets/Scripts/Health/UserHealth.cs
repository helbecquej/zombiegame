﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserHealth : MonoBehaviour
{
    public int _maxHealth = 4;
    public int _currentHealth;

    public HealthBar _healthBar;

    private void Start()
    {
        _currentHealth = _maxHealth;
        _healthBar.SetMaxHealth(_maxHealth);
    }

    public void TakeDamage(int damage)
    {
        _currentHealth -= damage;
        _healthBar.SetHealth(_currentHealth);
    }
}
