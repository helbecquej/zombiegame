﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    [SerializeField]
    private Transform _camera;

    //called just after regular update
    void LateUpdate()
    {
        transform.LookAt(transform.position + _camera.forward);
    }
}
