﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField]
    float _mouseSensitivity = 100f;
    [SerializeField]
    Transform _playerBody;

    float _xRotation = 0f;

    void Start()
    {
        //lock the cursor at the center of the screen
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        // we multiply by Time.deltaTime so it doesn't depend on the frame rate : Time.deltaTime is the time between two call of the update function
        float mouseX = Input.GetAxis("Mouse X") * _mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * _mouseSensitivity * Time.deltaTime;

        _xRotation -= mouseY;
        //so the camera can't rotate behind the head and in the body
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);

        _playerBody.Rotate(Vector3.up * mouseX);
        transform.localRotation = Quaternion.Euler(_xRotation, 0f, 0f);
    }
}
