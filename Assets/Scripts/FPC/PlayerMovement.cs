﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    CharacterController _controller;
    [SerializeField]
    float _speed = 12f;
    [SerializeField]
    Transform _groundCheck;
    [SerializeField]
    float _groundDistance = 0.4f;
    [SerializeField]
    LayerMask _groundMask;
    [SerializeField]
    float _jumpHeight = 3f;

    float _gravity = -9.81f;

    Vector3 _velocity;
    bool _isGrounded;

    private void Start()
    {
        if (_controller == null)
        {
            _controller = gameObject.GetComponent<CharacterController>();

            if (_controller == null)
                Debug.LogError("The characterController of " + gameObject.name + " can't be found");
        }
    }

    void Update()
    {
        //creates a small sphere at the bottom of the player and if it collides with anything that is in our ground mask, isgrounded turns true
        _isGrounded = Physics.CheckSphere(_groundCheck.position, _groundDistance, _groundMask);

        if (_isGrounded && _velocity.y < 0f)
        {
            // not 0 so it still go to the ground even if it is detected sooner by the sphere
            _velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        _controller.Move(move * _speed * Time.deltaTime);

        //Jump
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
        }

        _velocity.y += _gravity * Time.deltaTime;
        // we multiply by time again because deltay = 1/2 * g * t²
        _controller.Move(_velocity * Time.deltaTime);
    }
}
