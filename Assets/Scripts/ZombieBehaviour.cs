﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieBehaviour : MonoBehaviour
{
    //Variables
    GameObject _mainPlayer;
    Animator _thisAnimator;

    //Inspector
    [SerializeField]
    float _minDistToChase = 1.0f;
    [SerializeField]
    float _minDistToAttack = 1.0f;
    [SerializeField]
    float _zombieChaseSpeed = 1.0f;

    void Start()
    {
        _mainPlayer = GameObject.FindGameObjectWithTag("Player");
        _thisAnimator = gameObject.GetComponent<Animator>();
        if (_thisAnimator == null)
        {
            Debug.LogError("The animator for the object " + gameObject.name + "was not found");
        }
        if (_mainPlayer == null)
        {
            Debug.LogError("The main player can't be found");
        }

    }

    void Update()
    {
        if (!_thisAnimator.GetBool("zombieIsDead") && _thisAnimator != null)
        {
            float distBetweenZombieAndPlayer = Vector3.Distance(gameObject.transform.position, _mainPlayer.transform.position);
            Vector3 target = new Vector3(_mainPlayer.transform.position.x, 0f, _mainPlayer.transform.position.z);
            if (distBetweenZombieAndPlayer <= _minDistToChase && distBetweenZombieAndPlayer > _minDistToAttack)
            {
                Debug.Log("chasing");
                _thisAnimator.SetBool("userIsCloseEnoughToChase", true);
                _thisAnimator.SetBool("userIsCloseEnoughToAttack", false);
                transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * _zombieChaseSpeed);
                transform.LookAt(target);
            }

            if (distBetweenZombieAndPlayer < _minDistToAttack)
            {
                Debug.Log("attacking");
                _thisAnimator.SetBool("userIsCloseEnoughToChase", false);
                _thisAnimator.SetBool("userIsCloseEnoughToAttack", true);
                //transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * _zombieChaseSpeed);
                transform.LookAt(target);
            }

            if (distBetweenZombieAndPlayer > _minDistToChase)
            {
                Debug.Log("idle");
                _thisAnimator.SetBool("userIsCloseEnoughToChase", false);
                _thisAnimator.SetBool("userIsCloseEnoughToAttack", false);
            }
        }
    }
}
